package uk.ac.leedsbeckett.enigma.enigmaandroidsecurity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ActivityOptions;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/*
Main Activity: The activity users access once they have logged in
 */
public class MainActivity extends AppCompatActivity {

    final String TAG = "Main Activity"; // Activity tag for logging

    // Called on activity startup
    @Override
    @RequiresApi(api = Build.VERSION_CODES.P)
    protected void onCreate(Bundle savedInstanceState) {

        ActivityOptions options = ActivityOptions.makeBasic(); // Allow lock task mode in the options
        options.setLockTaskEnabled(true); // Enable lock task mode functionality
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button lockButton = (Button) findViewById(R.id.button_lock); // Lock button on click listener
        lockButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                lock();
            }
        });
        Button unlockButton = (Button) findViewById(R.id.button_unlock); // Unlock button on click listener
        unlockButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                unlock();
            }
        });
    }

    /// Used to lock the device
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void lock(){
        Log.d(TAG, "LOCK");
        findViewById(R.id.textView_locked).setVisibility(View.VISIBLE);
        startLockTask();
    }

    /// Used to unlock the device
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void unlock(){
        Log.d(TAG, "UNLOCK");
        findViewById(R.id.textView_locked).setVisibility(View.INVISIBLE);
        stopLockTask();
    }
}